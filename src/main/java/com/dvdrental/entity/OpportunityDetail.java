package com.dvdrental.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "OpportunityDetail")
public class OpportunityDetail {
	private static final long serialVersionUID = -3009157732242241606L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="detailsID")
	private long detailsID;

	@Column(name = "oppotunityName")
	private String oppotunityName;

	@Column(name = "projectOpen")
	private boolean projectOpen;
	
	@Column(name = "modifiedOn", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;
	
    @OneToMany(cascade = CascadeType.ALL)
	List<OpportunityStep> steps;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private Opportunity opportunity;
    
	public OpportunityDetail() {
		super();
	}

	
	public long getDetailsID() {
		return detailsID;
	}

	public void setDetailsID(long detailsID) {
		this.detailsID = detailsID;
	}

	public String getOppotunityName() {
		return oppotunityName;
	}

	public void setOppotunityName(String oppotunityName) {
		this.oppotunityName = oppotunityName;
	}

	public boolean isProjectOpen() {
		return projectOpen;
	}

	public void setProjectOpen(boolean projectOpen) {
		this.projectOpen = projectOpen;
	}

	public List<OpportunityStep> getSteps() {
		return steps;
	}

	public void setSteps(List<OpportunityStep> steps) {
		this.steps = steps;
	}

	public Opportunity getOpportunity() {
		return opportunity;
	}

	public void setOpportunity(Opportunity opportunity) {
		this.opportunity = opportunity;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "OpportunityDetail [detailsID=" + detailsID +
				", oppotunityName=" + oppotunityName + ", projectOpen=" +
				projectOpen + ", steps=" + steps + ", opportunity=" +
				opportunity + "]";
	}

	
}
