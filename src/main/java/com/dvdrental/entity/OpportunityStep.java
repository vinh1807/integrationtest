package com.dvdrental.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "OpportunityStep")
public class OpportunityStep {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long stepsID;

	@Column(name="stepSequence")
	private int stepSequence;
	
	@Column(name = "stepName")
	private String stepName;

	@Column(name = "isEnable")
	private boolean isEnable;
	
	@Column(name = "modifiedOn", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "detailsID")
	private OpportunityDetail opportunityDetail;

    public OpportunityStep() {}
	public OpportunityStep(long stepsID, int stepSequence, String stepName,
			boolean isEnable, OpportunityDetail opportunityDetail) {
		super();
		this.stepsID = stepsID;
		this.stepSequence = stepSequence;
		this.stepName = stepName;
		this.isEnable = isEnable;
		this.opportunityDetail = opportunityDetail;
	}

	public long getStepsID() {
		return stepsID;
	}

	public void setStepsID(long stepsID) {
		this.stepsID = stepsID;
	}

	public int getStepSequence() {
		return stepSequence;
	}

	public void setStepSequence(int stepSequence) {
		this.stepSequence = stepSequence;
	}

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public boolean isEnable() {
		return isEnable;
	}

	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	public OpportunityDetail getOpportunityDetail() {
		return opportunityDetail;
	}

	public void setOpportunityDetail(OpportunityDetail opportunityDetail) {
		this.opportunityDetail = opportunityDetail;
	}

    
}
