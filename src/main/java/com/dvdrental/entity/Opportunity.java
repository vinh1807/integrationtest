package com.dvdrental.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Opportunity")
public class Opportunity {

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "Account", nullable = false)
	private String Account;

	@Column(name = "projectType", nullable = false)
	private String projectType;

	@Column(name = "asseessmentDueDate", nullable = true)
	private Date asseessmentDueDate;

	@Column(name = "opportunityStage", nullable = true)
	private String opportunityStage;

	@Column(name = "proposalSentToClient", nullable = true)
	private Boolean proposalSentToClient;

	@Column(name = "packageType", nullable = true)
	private String packageType;

	@Column(name = "estimatedRevenue", nullable = false)
	private double estimatedRevenue;

	@Column(name = "estLastDayOnSite", nullable = false)
	private Date estLastDayOnSite;
	
	@Column(name = "modifiedOn", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	public Long getEid() {
		return id;
	}

	public void setEid(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return Account;
	}

	public void setAccount(String account) {
		Account = account;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public Date getAsseessmentHueDate() {
		return asseessmentDueDate;
	}

	public void setAsseessmentHueDate(Date asseessmentDueDate) {
		this.asseessmentDueDate = asseessmentDueDate;
	}

	public String getOpportunityStage() {
		return opportunityStage;
	}

	public void setOpportunityStage(String opportunityStage) {
		this.opportunityStage = opportunityStage;
	}

	public Boolean getProposalSentToClient() {
		return proposalSentToClient;
	}

	public void setProposalSentToClient(Boolean proposalSentToClient) {
		this.proposalSentToClient = proposalSentToClient;
	}

	public String getPackageType() {
		return packageType;
	}

	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}

	public double getEstimatedRevenue() {
		return estimatedRevenue;
	}

	public void setEstimatedRevenue(double estimatedRevenue) {
		this.estimatedRevenue = estimatedRevenue;
	}

	public Date getEstLastDayOnSite() {
		return estLastDayOnSite;
	}

	public void setEstLastDayOnSite(Date estLastDayOnSite) {
		this.estLastDayOnSite = estLastDayOnSite;
	}

}