package com.dvdrental.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dvdrental.entity.Actor;
import com.dvdrental.service.ActorService;

@RestController
@RequestMapping("actor")
public class ActorController {

	@Autowired
	private ActorService actorService;

	@PostMapping("/")
	public ResponseEntity<?> saveActor(@RequestBody Actor actor) {
		actorService.saveActor(actor);
		return ResponseEntity.ok().body(actor);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Actor> getActorById(@PathVariable("id") int id) {
		Actor actor = actorService.getActorById(id);
		return ResponseEntity.ok().body(actor);
	}
	
	@GetMapping("/name/{name}")
	public ResponseEntity<List<Actor>> getActorById(@PathVariable("name") String name) {
		List<Actor> actor = actorService.getActorByName(name);
		return ResponseEntity.ok().body(actor);
	}

	@GetMapping("/")
	public ResponseEntity<Page<Actor>> getAllActorsByPage(@RequestParam(value = "orderBy", defaultValue = "actorId") String orderBy,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
		Page<Actor> actors = actorService.getAllActorsByPage(orderBy, page, pageSize);
		return ResponseEntity.ok().body(actors);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteActor(@PathVariable("id") int id) {
		actorService.deleteActor(id);
		return ResponseEntity.ok().body(id);
	}

	@PutMapping("/")
	public ResponseEntity<?> update(@RequestBody Actor actor) {
		actorService.updateActor(actor);
		return ResponseEntity.ok().body(actor);
	}

}
