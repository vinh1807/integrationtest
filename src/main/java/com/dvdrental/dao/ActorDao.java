package com.dvdrental.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dvdrental.entity.Actor;

public interface ActorDao extends JpaRepository<Actor, Integer> {
	
	List<Actor> findByFirstNameContainingOrLastNameContainingAllIgnoreCase(String firstName, String lastName);

}
