package com.dvdrental.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.dvdrental.entity.Actor;

public interface ActorService {
	
	void 			saveActor(Actor actor);
	
	List<Actor> 	getAllActors();

	Page<Actor>		getAllActorsByPage(String orderBy, int page, int pageSize);
	
	Actor			getActorById(int id);
	
	List<Actor> 	getActorByName(String name);
	
	void 			updateActor(Actor actor);
	
	void 			deleteActor(int actorId);
	
}
