package dvdrental.integrationtest;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.*;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dvdrental.SpringdvdrentalApplication;
import com.dvdrental.entity.Actor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@Category(IIntergrationTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SpringdvdrentalApplication.class,
	webEnvironment = WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({ 
	DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@TestPropertySource(locations = "classpath:test.properties")
@DatabaseSetup("classpath:actorData.xml")
public class IntegrationTest {

	String actorWillReturnAfterSaved;
	ObjectMapper mapper = new ObjectMapper();

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() throws JsonProcessingException {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		Actor actorWillBeSaved = new Actor("Vinh", "Le");
		actorWillBeSaved.setActorId(3);

		actorWillReturnAfterSaved = mapper.writeValueAsString(actorWillBeSaved);
	}

	@Test
	@ExpectedDatabase(value = "classpath:actorData.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testGetActorById() throws Exception {
		mockMvc.perform(get("/actor/1")).andExpect(status().isOk()).andExpect(content()
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.firstName", is("Mike")));
	}
	
	@Test
	@ExpectedDatabase(value = "classpath:actorData.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testGetAllActors() throws Exception {
		mockMvc.perform(get("/actor/")).andExpect(status().isOk()).andExpect(content()
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.content", hasSize(2)))
			.andExpect(jsonPath("$.content[0].firstName", is("Mike")));
	}
	
	@Test
	@ExpectedDatabase(value = "classpath:actorData.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testGetAllActorsByPage() throws Exception {
		mockMvc.perform(get("/actor/")).andExpect(status().isOk()).andExpect(content()
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.content", hasSize(2)))
			.andExpect(jsonPath("$.content[0].firstName", is("Mike")));
	}

	@Test
	@ExpectedDatabase(value = "classpath:actorData-add-expected.xml",
		assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testSaveActor() throws Exception {
		Actor actor = new Actor("Vinh", "Le");
		String actorWillBeSaved = mapper.writeValueAsString(actor);

		mockMvc.perform(post("/actor/").contentType(MediaType.APPLICATION_JSON)
			.content(actorWillBeSaved)).andExpect(status().isOk())
			.andExpect(jsonPath("$.firstName", is("Vinh")))
			.andExpect(jsonPath("$.lastName", is("Le")));
	}

	@Test
	@ExpectedDatabase(value = "classpath:actorData-deleted-expected.xml",
		assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDeleteActor() throws Exception {
		mockMvc.perform(delete("/actor/1")).andExpect(status().isOk());
	}

	@Test
	@ExpectedDatabase(value = "classpath:actorData-updated-expected.xml",
		assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdateActor() throws Exception {
		Actor actor = new Actor("NewPurcell", "NewDominic");
		actor.setActorId(2);
		String jsonActorUpdate = mapper.writeValueAsString(actor);

		mockMvc.perform(put("/actor/").contentType(MediaType.APPLICATION_JSON)
				.content(jsonActorUpdate)).andExpect(status().isOk());
	}

}
