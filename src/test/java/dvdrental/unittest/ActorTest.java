package dvdrental.unittest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dvdrental.SpringdvdrentalApplication;
import com.dvdrental.entity.Actor;
import com.dvdrental.service.ActorService;

@Category(UnitTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=SpringdvdrentalApplication.class)
public class ActorTest {

	@Autowired
	private ActorService actorService;

	@Test
	public void testGetActorById() {
		Actor actor = new Actor("vinh", "le tran");
		actorService.saveActor(actor);
		
		Actor fromDB = actorService.getActorById(actor.getActorId());
		assertEquals(actor.getFirstName(), fromDB.getFirstName());
	}

	@Test
	public void testGetActorByName() {
		Actor actor = new Actor("vinh", "le tran");
		actorService.saveActor(actor);
		Actor actor1 = new Actor("waee", "le tran");
		actorService.saveActor(actor1);

		List<Actor> fromDBs = actorService.getActorByName("vinh");
		for (Actor fromDB : fromDBs) {
			assertTrue(fromDB.getFirstName().toLowerCase().contains("vinh"));
		}
	}

	@Test
	public void testSaveActor() {
		Actor actor = new Actor("vinh", "le tran");

		assertEquals(0, actor.getActorId());
		actorService.saveActor(actor);
		assertNotEquals(0, actor.getActorId());
		Actor fromDB = actorService.getActorById(actor.getActorId());
		assertEquals(actor.getFirstName(), fromDB.getFirstName());
		assertEquals(actor.getLastName(), fromDB.getLastName());
	}
	
	@Test
	public void testUpdateActor() {
		Actor actor = new Actor("vinh", "le tran");
		actorService.saveActor(actor);
		
		actor.setFirstName("hniv");
		actorService.updateActor(actor);
		
		Actor fromDB = actorService.getActorById(actor.getActorId());
		assertEquals("hniv", fromDB.getFirstName());
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testDeleteActor() {
		Actor actor = new Actor("vinh", "le tran");
		actorService.saveActor(actor);
		
		assertNotEquals(0, actor.getActorId());
		actorService.deleteActor(actor.getActorId());
	
		actorService.getActorById(actor.getActorId());
	}

	@Test
	public void testGetAllActor() {
		Actor actor = new Actor("vinh", "le tran");
		Actor actor1 = new Actor("vinh1", "le tran1");
		Actor actor2 = new Actor("vinh2", "le tran2");
		
		actorService.saveActor(actor);
		actorService.saveActor(actor1);
		actorService.saveActor(actor2);

		List<Actor> actorsFromDB = actorService.getAllActors();
		assertTrue(actorsFromDB.size() >= 3);
	}

	@Test
	public void whenInvalidName_thenReturnZeroList() {
		List<Actor> fromDBs = actorService.getActorByName("invalidname");
		assertEquals(0, fromDBs.size());
	}

	@Test(expected = NoSuchElementException.class)
	public void whenInvalidId_thenReturnNull() {
		actorService.getActorById(435345);
	}

}
